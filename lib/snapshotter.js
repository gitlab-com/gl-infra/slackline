"use strict";

const fetch = require("node-fetch");
const format = require("date-fns/format");
const { Storage } = require("@google-cloud/storage");
const logger = require("./logger");

function snapshotGrafanaUrl(alertName, grafanaUrl) {
  let dateFormatted = format(new Date(), "yyyy_MM_dd_HH_mm_ss_SSS");
  let safeName = alertName.replace(/[^\w_-]/g, "");
  let filename = `snapshot_${safeName}_${dateFormatted}.png`;

  return fetch(grafanaUrl, {
    headers: { Authorization: `Bearer ${process.env.GRAFANA_AUTH_TOKEN}` },
    follow: 10,
    timeout: 30000,
  })
    .catch((e) => {
      throw new Error(`Unable to fetch chart image from Grafana URL ${grafanaUrl}: ${e.message}: ${e.stack || e}`);
    })
    .then((res) => {
      if (!(res.status >= 200 && res.status < 300)) {
        throw new Error(`HTTP ${res.statusText}`);
      }

      const storage = new Storage();
      const myBucket = storage.bucket(process.env.GOOGLE_STORAGE_BUCKET);
      const file = myBucket.file(filename);

      return new Promise((resolve, reject) => {
        const dest = file.createWriteStream({
          predefinedAcl: "publicRead",
          metadata: {
            cacheControl: "public, max-age=31536000",
            contentType: res.headers.get("content-type"),
          },
        });

        res.body.pipe(dest).on("error", reject).on("finish", resolve);
      }).catch((e) => {
        throw new Error(`Unable to persist chart image to bucket: ${e.message}: ${e.stack || e}`);
      });
    })
    .then(() => {
      return `https://storage.googleapis.com/${process.env.GOOGLE_STORAGE_BUCKET}/${filename}`;
    })
    .catch((e) => {
      logger.logException({ message: "Unable to save snapshot image", grafanaUrl: grafanaUrl }, e);
      return null;
    });
}

module.exports = snapshotGrafanaUrl;
