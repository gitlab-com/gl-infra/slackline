"use strict";

const format = require("date-fns/format");

function getSilenceUrl(notification) {
  let pairs = Object.keys(notification.groupLabels).map((key) => `${key}="${notification.groupLabels[key]}"`);
  let filterString = encodeURIComponent(`{${pairs.join(", ")}}`);
  return `${process.env.ALERTMANAGER_URL}#/silences/new?filter=${filterString}`;
}

function getNewIssueUrl(notification, context) {
  let chartMarkdown = context.chartUrl ? `![chart](${context.chartUrl})` : "";
  let label = context.serviceDetails.getLabel();
  let labelMarkdown = label ? `/label ~"${label}"` : "";
  let slackThreadMarkdown = context.slackPermalink ? `**Slack Thread**: ${context.slackPermalink}\n` : "";

  let description = `
${slackThreadMarkdown}${chartMarkdown}

/label ~incident
${labelMarkdown}
`;

  let title = `${format(new Date(), "yyyy-MM-dd")}: ${context.title}`;

  return `https://gitlab.com/gitlab-com/gl-infra/production/issues/new?issue[description]=${encodeURIComponent(description)}&issue[title]=${encodeURIComponent(
    title
  )}`;
}

function buildTakeActionAttachment(notification, context) {
  let attachment = {
    title: "🎬 Take Action...",
    actions: [
      {
        text: "🚨 Open Issue",
        type: "button",
        style: "danger",
        url: getNewIssueUrl(notification, context),
      },
    ],
  };

  return attachment;
}

module.exports = { buildTakeActionAttachment, getSilenceUrl };
