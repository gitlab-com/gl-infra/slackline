"use strict";

const { sendFiringAlertToSlack, sendResolvedAlertToSlack, sendResolvedThreadToSlack } = require("./send-alert-to-slack");
const { findNotification, persistNotification, closeNotification } = require("./persist-notification");
const logger = require("./logger");

async function notificationHandler(notification, requestURL) {
  if (!notification || !notification.alerts) return;

  logger.info({
    message: "Incoming notification",
    notification: notification,
    searchParams: requestURL.searchParams.toString(),
  });

  let title = notification.commonAnnotations.title;
  if (!title) {
    // Ignore weird phantom alerts from alertmanager
    return;
  }

  // Check if we have already seen this alert before and dedupe
  let [id, source] = (await findNotification(notification)) || [];

  if (notification.status === "firing") {
    if (source) {
      logger.info({
        message: "Duplicate alert. Ignoring",
        originalSource: source,
      });
      return;
    }

    let messageResponse = await sendFiringAlertToSlack(notification, requestURL);
    await persistNotification(notification, messageResponse);
    return;
  }

  // Handle resolved messages
  const parentMessage = source && source.slack;
  if (!parentMessage) {
    logger.warn({
      message: "Unable to find original slack message for resolution, will send second message",
      group_key: notification.group_key,
    });

    return sendResolvedAlertToSlack(notification, requestURL);
  }

  // Add a notification to the thread and mark the incident as closed
  await Promise.all([sendResolvedThreadToSlack(notification, parentMessage), closeNotification(notification, id, source)]);
}

module.exports = notificationHandler;
