"use strict";

const url = require("url");
const notificationUtils = require("./notification-utils");
const getMinutes = require("date-fns/getMinutes");
const setMilliseconds = require("date-fns/setMilliseconds");
const setSeconds = require("date-fns/setSeconds");
const setMinutes = require("date-fns/setMinutes");

const DEFAULT_MIN_ZOOM_HOURS = 2;

function roundUp(d) {
  d = new Date(d); // clone
  let mins = getMinutes(d);
  let roundUpBy = mins % 15 == 0 ? 0 : 15 - (mins % 15);

  d = setMinutes(d, mins + roundUpBy);
  d = setSeconds(d, 0);
  d = setMilliseconds(d, 0);

  return d;
}

function roundDown(d) {
  d = new Date(d); // clone

  let mins = getMinutes(d);
  let roundDownBy = mins % 15;

  d = setMinutes(d, mins - roundDownBy);
  d = setSeconds(d, 0);
  d = setMilliseconds(d, 0);

  return d;
}

function getNotificationPeriod(notification, performRoundUp) {
  let grafanaMinimumZoomHours = notification.commonAnnotations.grafana_min_zoom_hours;
  grafanaMinimumZoomHours = parseFloat(grafanaMinimumZoomHours) || DEFAULT_MIN_ZOOM_HOURS;
  let grafanaMinimumZoomMS = grafanaMinimumZoomHours * 60 /* minutes */ * 60 /* seconds */ * 1000; /* ms */

  let now = Date.now();
  let startsAt = notificationUtils.earliestAlertStart(notification) || new Date(now - 2 * 60 * 60 * 1000);
  let latestEnd = notificationUtils.lastestAlertEnd(notification) || now;

  let twoMinutesAfter = latestEnd.valueOf() + 2 * 60 * 1000;
  let endsAt = now < twoMinutesAfter ? now : twoMinutesAfter;

  let from = startsAt.valueOf();
  let to = endsAt;

  if (to - from < grafanaMinimumZoomMS) {
    from = to - grafanaMinimumZoomMS;
  }

  return [roundDown(from).valueOf(), performRoundUp ? roundUp(to).valueOf() : to];
}

function grafanaLink(host, prefix, extraQueryVars, notification, performRoundUp) {
  let grafanaDashboardId = notification.commonAnnotations.grafana_dashboard_id;
  if (!grafanaDashboardId) {
    return;
  }

  let grafanaPanelId = notification.commonAnnotations.grafana_panel_id;

  // First check the common annotations for a datasource ID.
  let grafanaDatasourceId = notification.commonAnnotations.grafana_datasource_id;

  // If one is not found, use the first one we find in the alerts.
  if (!grafanaDatasourceId) {
    for (const alert of notification.alerts) {
      if (alert.annotations.grafana_datasource_id) {
        grafanaDatasourceId = alert.annotations.grafana_datasource_id;
      }
    }
  }

  if (grafanaDatasourceId) {
    extraQueryVars["var-PROMETHEUS_DS"] = grafanaDatasourceId;
  }

  let [from, to] = getNotificationPeriod(notification, performRoundUp);
  let query = Object.assign(
    {
      from: from,
      to: to,
      panelId: grafanaPanelId,
      tz: "UTC",
    },
    extraQueryVars
  );

  return buildGrafanaLink(host, `${prefix}/${grafanaDashboardId}`, query, notification.commonAnnotations.grafana_variables, notification);
}

function buildGrafanaLink(host, path, query, grafanaVariables, notification) {
  if (grafanaVariables) {
    let vars = grafanaVariables.split(/\s*,\s*/);
    vars.reduce((memo, v) => {
      let value = notification.commonLabels[v] || notification.groupLabels[v];
      memo[`var-${v}`] = value || "";
      return memo;
    }, query);
  }

  return url.format({
    protocol: "https",
    host: host,
    pathname: path,
    query: query,
  });
}

function getGrafanaSnapshotUrlForAlert(notification) {
  return grafanaLink(
    "dashboards.gitlab.net",
    "/render/d-solo",
    {
      theme: "dark",
      width: 800,
      height: 600,
      timeout: 60,
    },
    notification,
    false
  );
}

function getGrafanaUrl(notification) {
  return grafanaLink("dashboards.gitlab.net", "/d", {}, notification, true);
}

function getComponentTriageGrafanaUrl(notification) {
  if (!notification.commonLabels.environment && !notification.groupLabels.environment) return;
  if (!notification.commonLabels.type && !notification.groupLabels.type) return;

  return buildGrafanaLink("dashboards.gitlab.net", "/d/VE4pXc1iz/general-triage-components", {}, "environment,type", notification);
}

function getServiceTriageGrafanaUrl(notification) {
  if (!notification.commonLabels.environment && !notification.groupLabels.environment) return;
  if (!notification.commonLabels.type && !notification.groupLabels.type) return;

  return buildGrafanaLink("dashboards.gitlab.net", "/d/general-service/general-service-platform-metrics", {}, "environment,type", notification);
}

module.exports = {
  buildGrafanaLink: buildGrafanaLink,
  getComponentTriageGrafanaUrl: getComponentTriageGrafanaUrl,
  getServiceTriageGrafanaUrl: getServiceTriageGrafanaUrl,

  getGrafanaSnapshotUrlForAlert: getGrafanaSnapshotUrlForAlert,
  getGrafanaUrl: getGrafanaUrl,
  grafanaLink: grafanaLink,
};
