"use strict";

const parseISO = require("date-fns/parseISO");

/** Parse a date taking prom idosyncracies into account */
function safeParsePromData(date) {
  if (!date || date === "0001-01-01T00:00:00Z") return null;
  return parseISO(date);
}

/** Returns the minimal non-null value out of the two params or null if they are both null */
function minNonNull(a, b) {
  if (!a) return b;
  if (!b) return null;
  if (a < b) return a;
  return b;
}

/** Returns the maximum non-null value out of the two params or null if they are both null */
function maxNonNull(a, b) {
  if (!a) return b;
  if (!b) return null;
  if (a < b) return a;
  return b;
}

function earliestAlertStart(notification) {
  return notification.alerts.reduce((min, alert) => {
    let startsAt = safeParsePromData(alert.startsAt);
    return minNonNull(min, startsAt);
  }, null);
}

function lastestAlertEnd(notification) {
  return notification.alerts.reduce((max, alert) => {
    let endsAt = safeParsePromData(alert.endsAt);
    return maxNonNull(max, endsAt);
  }, null);
}

module.exports = {
  earliestAlertStart,
  lastestAlertEnd,
};
