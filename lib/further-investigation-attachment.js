"use strict";

const promqlTemplate = require("./promql-template");
const url = require("url");

function buildFurtherInvestigationAttachment(notification, context) {
  let links = [];

  let serviceDetails = context.serviceDetails;

  let runbookUrl = notification.commonAnnotations.runbook;
  if (runbookUrl) {
    links.push({
      text: `📚 ${notification.commonAnnotations.runbook_title || "Runbook"}`,
      type: "button",
      url: `https://gitlab.com/gitlab-com/runbooks/blob/master/${runbookUrl}`,
    });
  }

  for (let i = 1; i <= 5; i++) {
    let title = notification.commonAnnotations[`link${i}_title`];
    let url = notification.commonAnnotations[`link${i}_url`];

    if (title && url) {
      links.push({
        type: "button",
        text: title,
        url: url,
      });
    }
  }

  let gitlabDashboard = serviceDetails.getGitLabDashboard();
  if (gitlabDashboard) {
    links.push({
      text: `📊 GitLab Dashboard`,
      type: "button",
      url: gitlabDashboard,
    });
  }

  let grafanaFolder = serviceDetails.getGrafanaFolder();
  if (grafanaFolder) {
    links.push({
      text: "📈 More Dashboards",
      type: "button",
      url: `https://dashboards.gitlab.net/dashboards/f/${grafanaFolder}`,
    });
  }

  links.push(getFurtherInvestigationLinks(notification));

  let attachments = [];

  // Slack allows 5 buttons per attachment, so group them into groups of 5
  let count = 0;
  while (links.length) {
    count++;

    let attachment = {
      title: count === 1 ? "🕵️ Further Investigation..." : "",
      actions: [],
    };
    attachments.push(attachment);

    for (let i = 0; i < 5 && links.length; i++) {
      let link = links.shift();
      attachment.actions.push(link);
    }
  }

  let loggingLinks = serviceDetails.getLoggingLinks();
  if (loggingLinks && loggingLinks.length) {
    attachments.push({
      title: "🦌 Logging",
      text: loggingLinks.map((loggingLink) => `• <${loggingLink.permalink}|${loggingLink.name}>`).join("\n"),
    });
  }

  return attachments;
}

function getFurtherInvestigationLinks(notification) {
  const links = [];

  // Loop over the alerts and build links for ones that support Grafana explore.
  for (const alert of notification.alerts) {
    if (alert.annotations.grafana_datasource_id) {
      links.push(buildAlertLink(alert));
    }
  }

  // If we haven't found any Grafana explore links, fall back to a Thanos one.
  if (links.length !== 0 && notification.commonAnnotations.promql_template_1) {
    links.push(buildGrafanaQueryLink(notification));
  } else if (notification.commonAnnotations.promql_template_1) {
    links.push(buildThanosQueryLink(notification));
  }

  return links;
}

function buildAlertLink(alert) {
  return {
    text: "📈 Alert",
    type: "button",
    url: url.format({
      hostname: "dashboards.gitlab.net",
      pathname: "/explore",
      protocol: "https",
      query: {
        schemaVersion: 1,
        orgId: 1,
        panes: JSON.stringify({
          "00v": {
            datasource: alert.annotations.grafana_datasource_id,
            queries: [
              {
                refId: "A",
                expr: new url.URL(alert.generatorURL).searchParams.get("g0.expr"),
                range: true,
                instant: true,
                datasource: { type: "prometheus", uid: alert.annotations.grafana_datasource_id },
                editorMode: "code",
              },
            ],
            range: { from: "now-1h", to: "now" },
          },
        }),
      },
    }),
  };
}

function buildGrafanaQueryLink(notification) {
  let panes = {};
  if (notification.commonAnnotations.promql_template_1) {
    panes["yzb"] = buildPane(notification.alerts[0].annotations.grafana_datasource_id, notification.commonAnnotations.promql_template_1, notification);
  }
  if (notification.commonAnnotations.promql_template_2) {
    panes["6nt"] = buildPane(notification.alerts[0].annotations.grafana_datasource_id, notification.commonAnnotations.promql_template_2, notification);
  }

  return {
    text: "📈 Query",
    type: "button",
    url: url.format({
      hostname: "dashboards.gitlab.net",
      pathname: "/explore",
      protocol: "https",
      query: {
        schemaVersion: 1,
        orgId: 1,
        panes: JSON.stringify(panes),
      },
    }),
  };
}

function buildPane(dataSourceId, expression, notification) {
  return {
    datasource: dataSourceId,
    queries: [
      {
        refId: "A",
        expr: promqlTemplate(expression, notification),
        range: true,
        instant: true,
        datasource: { type: "prometheus", uid: dataSourceId },
        editorMode: "code",
      },
    ],
    range: { from: "now-1h", to: "now" },
  };
}

function buildThanosQueryLink(notification) {
  const query = {};
  let count = 0;
  for (let i = 1; i <= 5; i++) {
    let template = notification.commonAnnotations[`promql_template_${i}`];
    let rendered = promqlTemplate(template, notification);
    if (rendered) {
      query[`g${count}.range_input`] = "6h";
      query[`g${count}.expr`] = rendered;
      query[`g${count}.tab`] = "0";
      count++;
    }
  }

  return {
    text: "📈 Query",
    type: "button",
    url: url.format({
      hostname: "thanos.gitlab.net",
      pathname: "/graph",
      protocol: "https",
      query: query,
    }),
  };
}

module.exports = {
  buildFurtherInvestigationAttachment: buildFurtherInvestigationAttachment,

  // Exported for testing only:
  getFurtherInvestigationLinks: getFurtherInvestigationLinks,
};
