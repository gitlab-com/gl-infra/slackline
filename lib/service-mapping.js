"use strict";

var mappingData = require("../data/service-catalog.json");

function getServiceDetails(serviceName) {
  if (!serviceName) return;
  for (let service of mappingData.services) {
    if (service.name === serviceName) {
      return new ServiceDescriptor(serviceName, service);
    }
  }

  // Return a stub
  return new ServiceDescriptor(serviceName, {});
}

class ServiceDescriptor {
  constructor(serviceName, serviceDetails) {
    this.serviceName = serviceName;
    this.serviceDetails = serviceDetails;
  }

  getName() {
    return this.serviceName;
  }

  // This is the label for issues related to this service
  getLabel() {
    return this.serviceDetails.label;
  }

  // Primary Grafana Dashboard slug for this service
  getPrimaryGrafanaDashboard() {
    return (
      this.serviceDetails.observability && this.serviceDetails.observability.monitors && this.serviceDetails.observability.monitors.primary_grafana_dashboard
    );
  }

  // Dogfooding Monitoring - GitLab Dashboard for this service
  getGitLabDashboard() {
    return this.serviceDetails.observability && this.serviceDetails.observability.monitors && this.serviceDetails.observability.monitors.gitlab_dashboard;
  }

  // Grafana folder slug containing dashboards for
  getGrafanaFolder() {
    return this.serviceDetails.observability && this.serviceDetails.observability.monitors && this.serviceDetails.observability.monitors.grafana_folder;
  }

  getSentrySlug() {
    return this.serviceDetails.observability && this.serviceDetails.observability.monitors && this.serviceDetails.observability.monitors.sentry_slug;
  }

  // Returns array of [{ permalink, name }]
  getLoggingLinks() {
    return this.serviceDetails.technical && this.serviceDetails.technical.logging;
  }
}

module.exports = {
  getServiceDetails: getServiceDetails,
};
