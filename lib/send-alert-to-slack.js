"use strict";

const { WebClient, LogLevel } = require("@slack/web-api");
const { getGrafanaSnapshotUrlForAlert, getGrafanaUrl } = require("./grafana-link-builder");
const { buildFurtherInvestigationAttachment } = require("./further-investigation-attachment");
const { buildTakeActionAttachment, getSilenceUrl } = require("./take-action-attachment");
const snapshotter = require("./snapshotter");
const { getServiceDetails } = require("./service-mapping");
const operationTimer = require("./operation-timer");
const logger = require("./logger");
const chunk = require("lodash.chunk");

// Slack allows a max of 10 fields per block
const MAX_FIELDS_PER_BLOCK = 10;

const IGNORE_LABELS = new Set([
  "bound",
  "cluster",
  "env",
  "metric",
  "monitor",
  "period",
  "prometheus",
  "provider",
  "region",
  "replica",
  "rules_domain",
  "threshold_sigma",
  "tier",
  "slo_alert",
  "experimental",
]);

const labelEmojis = {
  alert_type: "🚦",
  alertname: "📛",
  environment: "🌆",
  feature_category: "🚀",
  queue: "🥋",
  severity: "📶",
  stage: "🎭",
  type: "🦺",
};

// Returns the channel that the slack notification will be sent to.
// By default, the receiver will use the value configured in the SLACK_DEFAULT_CHANNEL
// env-var, but callers can override it with the `channel` query parameter.
function getChannelForNotification(requestURL) {
  const channel = requestURL.searchParams.get("channel");

  if (!channel) {
    return process.env.SLACK_DEFAULT_CHANNEL;
  }

  if (channel.startsWith("#")) {
    return channel;
  } else {
    return `#${channel}`;
  }
}

const createContextForNotification = operationTimer.wrapAsync("slack-create-context", async function (notification) {
  let grafanaSnapshotUrl = getGrafanaSnapshotUrlForAlert(notification);
  let chartUrl =
    grafanaSnapshotUrl &&
    (await operationTimer.time("grafana-snapshotter", () => {
      return snapshotter("alert", grafanaSnapshotUrl);
    }));

  let title = notification.commonAnnotations.title || "Alert";

  let service = notification.commonLabels.type || notification.groupLabels.type;
  let serviceDetails = service && getServiceDetails(service);

  return {
    title: title,
    chartUrl: chartUrl,
    serviceDetails: serviceDetails,
  };
});

function commonLabelBlocks(notification) {
  let commonLabels = notification.commonLabels;
  let filteredLabels = Object.keys(commonLabels).filter((key) => !IGNORE_LABELS.has(key) && commonLabels[key]);

  let longValues = filteredLabels.filter((key) => String(commonLabels[key]).length >= 30);
  let shortValues = filteredLabels.filter((key) => String(commonLabels[key]).length < 30);

  let result = [];
  if (longValues.length) {
    result.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: longValues
          .map((key) => {
            let value = String(notification.commonLabels[key]);
            let emoji = labelEmojis[key] ? `${labelEmojis[key]} ` : "";
            return `${emoji}${key} \`${value}\``;
          })
          .join("\n"),
      },
    });
  }

  if (shortValues.length) {
    result = result.concat(
      chunk(shortValues, MAX_FIELDS_PER_BLOCK).map((keyChunk) => {
        return {
          type: "section",
          fields: keyChunk.map((key) => {
            let value = String(notification.commonLabels[key]);
            let emoji = labelEmojis[key];
            emoji = emoji ? `${emoji} ` : "";

            return {
              type: "mrkdwn",
              text: `${emoji}${key} \`${value}\``,
            };
          }),
        };
      })
    );
  }

  return result;
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function sendFiringAlertToSlack(notification, requestURL) {
  let context = await createContextForNotification(notification);

  let text = `*Alert Firing*: ${context.title}`;

  // An access token (from your Slack app or custom integration - xoxa, xoxp, or xoxb)
  const token = process.env.SLACK_TOKEN;
  const slackWeb = new WebClient(token, {
    logger: logger,
    logLevel: LogLevel.DEBUG,
  });

  let blocks = [
    { type: "section", text: { type: "mrkdwn", text: text } },
    { type: "section", text: { type: "mrkdwn", text: notification.commonAnnotations.description } },
  ];

  if (context.chartUrl) {
    blocks.push({
      type: "image",
      title: {
        type: "plain_text",
        text: "Grafana Snapshot",
      },
      image_url: context.chartUrl,
      alt_text: "Grafana Snapshot Chart",
    });
  }

  let chartLink = getGrafanaUrl(notification);
  if (chartLink) {
    blocks.push({
      type: "context",
      elements: [
        {
          type: "mrkdwn",
          text: "<" + chartLink + "|View in Grafana>",
        },
      ],
    });
  }

  blocks = blocks.concat(commonLabelBlocks(notification));

  let serviceDetails = context.serviceDetails;

  let primaryActions = [];

  let primaryGrafanaDashboard = serviceDetails.getPrimaryGrafanaDashboard();
  if (primaryGrafanaDashboard) {
    let stage = notification.commonLabels["stage"];
    primaryActions.push({
      type: "button",
      text: {
        type: "plain_text",
        text: `:grafana: ${serviceDetails.getName()} Overview`,
        emoji: true,
      },
      style: "primary",
      url: `https://dashboards.gitlab.net/d/${primaryGrafanaDashboard}?var-stage=${stage}`,
    });
  }

  let silenceUrl = getSilenceUrl(notification);
  if (silenceUrl) {
    primaryActions.push({
      type: "button",
      text: {
        type: "plain_text",
        text: `:shushing_face: Silence Alert`,
        emoji: true,
      },
      style: "primary",
      url: silenceUrl,
    });
  }

  let sentrySlug = serviceDetails.getSentrySlug();
  if (sentrySlug) {
    primaryActions.push({
      type: "button",
      text: {
        type: "plain_text",
        text: `:sentry: Sentry`,
        emoji: true,
      },
      style: "primary",
      url: `https://sentry.gitlab.net/${sentrySlug}`,
    });
  }

  if (primaryActions.length) {
    blocks = blocks.concat([
      {
        type: "divider",
      },
      {
        type: "actions",
        elements: primaryActions,
      },
    ]);
  }

  blocks = blocks.concat([
    {
      type: "divider",
    },
    {
      type: "context",
      elements: [
        {
          type: "mrkdwn",
          text: "More actions and logs available :thread:",
        },
      ],
    },
  ]);

  let slackMessage = {
    text: text,
    blocks: blocks,
    channel: getChannelForNotification(requestURL),
  };

  logger.info({
    message: "slack post",
    slackMessage: slackMessage,
  });

  const MAX_RETRIES = parseInt(process.env.MAX_RETRIES, 10) || 3;
  const BACKOFF_SECONDS = parseInt(process.env.BACKOFF_SECONDS, 10) || 2;

  let messageResponse;

  for (let retries = 0; retries < MAX_RETRIES; retries++) {
    try {
      messageResponse = await operationTimer.time("slack-post", async () => {
        return await slackWeb.chat.postMessage(slackMessage);
      });
      break;
    } catch (e) {
      if (retries == MAX_RETRIES) {
        throw e;
      }

      logger.warn({
        message: "slack postMessage: exception caught",
        response: messageResponse,
        exception: e,
        retries: retries,
        sleep_seconds: BACKOFF_SECONDS ** (retries + 1),
      });
      await sleep(BACKOFF_SECONDS ** (retries + 1) * 1000);
    }
  }

  logger.info({
    message: "slack post response",
    response: messageResponse,
  });

  // If the thread fails, it should not affect the overall success
  try {
    let threadAttachments = [];

    let furtherInvestigationAttachments = buildFurtherInvestigationAttachment(notification, context);
    if (furtherInvestigationAttachments) {
      threadAttachments = threadAttachments.concat(furtherInvestigationAttachments);
    }

    threadAttachments.push(buildTakeActionAttachment(notification, context));

    await operationTimer.time("thread-post", async () => {
      return await slackWeb.chat.postMessage({
        text: "",
        channel: messageResponse.channel,
        thread_ts: messageResponse.ts,
        attachments: threadAttachments,
      });
    });
  } catch (e) {
    logger.logException({ message: "unable to add thread details, ignoring" }, e);
  }

  return messageResponse;
}

async function sendResolvedThreadToSlack(notification, parentMessage) {
  let context = await createContextForNotification(notification);

  let attachments = [
    {
      title: "Resolved",
      image_url: context.chartUrl,
      title_link: getGrafanaUrl(notification),
      fields: [],
      actions: [],
      mrkdwn_in: ["text"],
    },
  ];

  let text = `*Alert Resolved*: ${context.title}`;

  // An access token (from your Slack app or custom integration - xoxa, xoxp, or xoxb)
  const token = process.env.SLACK_TOKEN;
  const slackWeb = new WebClient(token, {
    logger: logger,
    logLevel: LogLevel.DEBUG,
  });

  await slackWeb.chat.postMessage({
    text: text,
    channel: parentMessage.channel,
    thread_ts: parentMessage.ts,
    attachments: attachments,
  });

  try {
    await slackWeb.reactions.add({
      name: "white_check_mark",
      channel: parentMessage.channel,
      timestamp: parentMessage.ts,
    });
  } catch (e) {
    logger.logException({ message: "Unable to update reaction on thread" }, e);
  }

  return;
}

async function sendResolvedAlertToSlack(notification, requestURL) {
  let context = await createContextForNotification(notification);

  let text = `*Alert Resolved*: ${context.title}`;

  let blocks = [{ type: "section", text: { type: "mrkdwn", text: text } }];

  if (context.chartUrl) {
    blocks.push({
      type: "image",
      title: {
        type: "plain_text",
        text: "Grafana Snapshot",
      },
      image_url: context.chartUrl,
      alt_text: "Grafana Snapshot Chart",
    });
  }

  let chartLink = getGrafanaUrl(notification);
  if (chartLink) {
    blocks.push({
      type: "context",
      elements: [
        {
          type: "mrkdwn",
          text: "<" + chartLink + "|View in Grafana>",
        },
      ],
    });
  }

  blocks = blocks.concat(commonLabelBlocks(notification));

  // An access token (from your Slack app or custom integration - xoxa, xoxp, or xoxb)
  const token = process.env.SLACK_TOKEN;
  const slackWeb = new WebClient(token, {
    logger: logger,
    logLevel: LogLevel.DEBUG,
  });

  // No thread, post in the main channel
  let slackMessage = {
    text: text,
    blocks: blocks,
    channel: getChannelForNotification(requestURL),
  };

  logger.info({
    message: "sending resolved message to slack",
    slackMessage: slackMessage,
  });
  let response = await slackWeb.chat.postMessage(slackMessage);

  try {
    await slackWeb.reactions.add({
      name: "white_check_mark",
      channel: response.channel,
      timestamp: response.ts,
    });
  } catch (e) {
    logger.logException({ message: "Unable to update reaction on thread" }, e);
  }

  return;
}

module.exports = {
  sendFiringAlertToSlack: operationTimer.wrapAsync("slack-post-firing", sendFiringAlertToSlack),
  sendResolvedAlertToSlack: operationTimer.wrapAsync("slack-post-resolved", sendResolvedAlertToSlack),
  sendResolvedThreadToSlack: operationTimer.wrapAsync("slack-post-resolved-thread", sendResolvedThreadToSlack),

  // Exported for testing only:
  commonLabelBlocks: commonLabelBlocks,
};
