"use strict";

const logger = require("./logger");

async function time(operationName, fn) {
  let startTime = Date.now();
  let succesful = false;
  try {
    let result = fn();
    if (!result) {
      logger.warn({
        message: "timing: Timer returned no response, may not be chaining promises correctly",
        operationName: operationName,
      });
    }
    const resolvedResult = await result;
    succesful = true;
    return resolvedResult;
  } catch (e) {
    logger.error({
      message: "timing-fail",
      operationName: operationName,
    });

    throw e;
  } finally {
    let duration = Date.now() - startTime;

    logger.info({
      message: "timing",
      succesful: succesful,
      operationName: operationName,
      duration_s: duration / 1000,
    });
  }
}

function wrapAsync(operationName, fn) {
  return function () {
    const args = [...arguments];
    const t = this;

    return time(operationName, () => {
      return fn.apply(t, args);
    });
  };
}

module.exports = {
  time: time,
  wrapAsync: wrapAsync,
};
