"use strict";

const winston = require("winston");

const level = process.env.LOG_LEVEL || "info";

const logger = winston.createLogger({
  level: level,
  format: winston.format.json(),
  defaultMeta: {},
  transports: [new winston.transports.Console({ level: level })],
});

logger.logException = function (message, exception) {
  message.exception = {
    message: exception.message,
    stack: exception.stack,
  };
  logger.error(message);
};

// When passing a logger to the Slack client, expects `setName` and `setLevel` methods.
// Add them as null-ops
logger.setLevel = () => {};
logger.setName = () => {};

module.exports = logger;
