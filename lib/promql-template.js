"use strict";

function applyValues(promqlTemplate, notification) {
  if (!promqlTemplate) return;

  return promqlTemplate.replace(/\$([A-Za-z0-9_-]+)/g, (_, key) => {
    return notification.commonLabels[key] || "";
  });
}

module.exports = applyValues;
