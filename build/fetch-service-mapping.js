/* eslint-disable no-process-exit */
/* eslint-disable node/no-unpublished-require */
"use strict";

const fetch = require("node-fetch");
const yaml = require("js-yaml");
const fs = require("fs");
const util = require("util");
const logger = require("../lib/logger");
const fs_writeFile = util.promisify(fs.writeFile);

fetch(
  "https://gitlab.com/gitlab-com/runbooks/raw/master/services/service-catalog.yml"
)
  .then(res => res.text())
  .then(function(content) {
    var doc = yaml.load(content);
    return fs_writeFile(
      __dirname + "/../data/service-catalog.json",
      JSON.stringify(doc, null, "  ")
    );
  })
  .catch(err => {
    logger.logException({ message: "Unable to fetch service catalog" }, err)
    process.exit(1);
  });
