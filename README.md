# Slackline

Slackline is a tiny bridge for customizing Prometheus AlertManager alerts for Slack.

It integrates the following components:

- **AlertManager**: where the alerts come from
- **Google Cloud Functions**: where Slackline runs
- **Grafana**: used to augment alerts with charts
- **Google Cloud Storage**: for storage of chart image snapshots

![Screenshot from Slack](doc/img/slack_screenshot.png)

## Developing

### Quick Start

```console
# Install dependencies
$ npm install

# Test
$ npm test
```

### Environment Setup

1. Install `asdf`: <https://asdf-vm.com/guide/getting-started.html>
1. Install `asdf` plugins: `scripts/install-asdf-plugins.sh`
1. Optional: obtain secrets for local development:
   1. Sign into 1password: `op signin gitlab.1password.com <YOUR_EMAIL_ADDRESS>`
   1. Log into 1password `eval "$(op signin gitlab)"`
   1. Allow direnv to run: `direnv allow`
1. Optional: install pre-commit hooks: `pre-commit install`

### Testing Locally

1. Start the cloud function locally with `npm start`
1. Open a second terminal (or `bg` the process)
1. Configure `SLACKLINE_BASE_URL` to point to the local service: `export SLACKLINE_BASE_URL=http://localhost:8080`. Note that if you are using `direnv`, this step will be done for you automatically.
1. Send an invocation to the local function with `npm integration-test`

```console
$ npm run integration-test
```
## Deploying

Slackline will deploy all changes from the main branch (currently `master`) into the staging (GCP project `gitlab-infra-automation-stg`) and production (GCP project `gitlab-infra-automation`).

As part of CI, changes on branches will also be pushed to the `test` environment. This uses the same project as staging (`gitlab-infra-automation-stg`), but a different lambda function name (`alertManagerBridgeTest`).

Once you have made your changes locally, you can push your code and let CI deploy your change to teh test environment.

Look at the `.gitlab-ci.yml` for how it handles the deployments and specifically the Environment Variables it uses for authenticating `gcloud` with dedicated GCP service accounts and pushing Slackline specific environment variables to Google Functions.

## Adding Grafana panels to your Slack alerts

In order to augment your Slack alerts with a Grafana panel you'll need to add three annotations:

- `grafana_dashboard_id`: This is the path to the Grafana dashboard. For example: `26q8nTzZz/service-platform-metrics`
- `grafana_panel_id`: This is the ID of the panel in Grafana. To find it, maximize a panel and look for the `panelId` querystring parameter in the Grafana URL.
- `grafana_variables`: comma-separated names for the prometheus labels that should be included in the Grafana URL as Grafana template variables.

For example:

```yaml
# Link panel 2 of
# dashboards.gitlab.net/26q8nTzZz/service-platform-metrics
# Passing through the `environment` and `type` labels as
# Grafana variables
groups:
  - name: service_availability.rules
    rules:
      - alert: widget_alert
        expr: ....
        for: 5m
        annotations:
          description: |
            Widgets are not being produced right now
          grafana_dashboard_id: "26q8nTzZz/service-platform-metrics"
          grafana_panel_id: "5"
          grafana_variables: "environment,type"
```

For real world examples, review the [GitLab alert configuration](https://gitlab.com/gitlab-com/runbooks/blob/master/alerts/general-service-alerts.yml).

## Configuration

Slackline uses environment variables for configuration. The configuration should look something like:

```yaml
BEARER_TOKEN: "TheSecretBearerToken"
GRAFANA_AUTH_TOKEN: "TokenToAccessGrafana"
GOOGLE_STORAGE_BUCKET: "GCS Bucket to upload snapshot images to"

SLACK_TOKEN: "xoxp-SLACK_TOKEN"
SLACK_DEFAULT_CHANNEL: "#alerts"
```

## Publishing messages to alternative channels

By default, messages will be delivered to the channel defined by the `SLACK_DEFAULT_CHANNEL` environment variable.

In some cases, it may be useful to allow messages to be delivered to alternative channels.

This can be done by adding a `channel` query parameter to the receiver URL in `alertmanager.yml`, for example:

```yaml
alertmanager:
  config:
    receivers:
    - name: slackline
      webhook_configs:
      - http_config:
          bearer_token: secret
        send_resolved: true
        url: https://xyz.cloudfunctions.net/alertManagerBridge?channel=alternative_alert_channel
```

In this case, the slack channel, `#alternative_alert_channel` will be used.

This allows Slackline to send alerts to multiple channels from a single instance of the GCF.

## Development Environment

### Install `asdf`

Full details at https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies.

### Install `asdf` plugins

Install `asdf` plugins and versions using `install-asdf-plugins.sh`:

```shell
scripts/install-asdf-plugins.sh
```

### Install dependencies

```shell
npm install
```

### Run tests

```shell
npm test
```

### Verify Source

Slackline uses `prettier` and `eslint` for source formatting and static analysis.

```shell
npm run verify
```

### Fix source verification issues

This will fix as many source verification issues as possible.

```shell
npm run fix
```

### Fix npm audit failures

If the `npm audit` fails during CI you may need to update the package dependencies:

```shell
npm audit fix
# or
npm audit fix --force
```
