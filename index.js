"use strict";

const notificationHandler = require("./lib/notification-handler");
const logger = require("./lib/logger");

function wrap(res, inner) {
  let statusCode = 0;
  const startTime = Date.now();
  return Promise.resolve()
    .then(() => {
      return inner();
    })
    .then(() => {
      statusCode = 200;
      res.status(statusCode).end();
    })
    .catch((err) => {
      logger.logException({ message: "request failed with exception" }, err);

      statusCode = err.httpResponseStatusCode || 500;
      res.status(statusCode).send(err.message);
    })
    .finally(() => {
      const duration = Date.now() - startTime;
      logger.info({
        message: "access",
        statusCode: statusCode,
        method: res.method,
        duration_s: duration / 1000,
      });
    });
}

async function incomingAlert(req) {
  let bearerToken = req.get("Authorization");
  if (!bearerToken) {
    let e = new Error("Unauthorized");
    e.httpResponseStatusCode = 401;
    throw e;
  }

  if (bearerToken !== `Bearer ${process.env.BEARER_TOKEN}`) {
    let e = new Error("Forbidden");
    e.httpResponseStatusCode = 403;
    throw e;
  }

  if (req.method !== "POST") {
    let e = new Error("Method Not Allowed");
    e.httpResponseStatusCode = 405;
    throw e;
  }

  let requestURL = new URL(req.url, `http://${req.headers.host}`);

  await notificationHandler(req.body, requestURL);
}

exports.alertManagerBridge = (req, res) => {
  wrap(res, () => {
    return incomingAlert(req);
  });
};

// Used for testing purposes only
exports.alertManagerBridgeTest = exports.alertManagerBridge;
