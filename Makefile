STAGING_PROJECT=gitlab-infra-automation-stg
PRODUCTION_PROJECT=gitlab-infra-automation
FUNCTION_NAME=alertManagerBridge
RUNTIME=nodejs20
REGION=us-central1
TIMEOUT=180s

.PHONY: all
all: validate test deploy-staging

.PHONY: validate
validate:
	npm run eslint

.PHONY: test
test:
	npm test

.PHONY: integration-test
integration-test:
	npm run integration-test

.PHONY: fetch-mapping
fetch-mapping:
	node build/fetch-service-mapping.js

.PHONY: deploy-test
deploy-test: fetch-mapping
	gcloud -q \
	--project $(STAGING_PROJECT) \
	functions deploy $(FUNCTION_NAME)Test \
	--region $(REGION) \
	--env-vars-file .env.staging.yaml \
	--runtime $(RUNTIME) \
	--trigger-http \
	--allow-unauthenticated

.PHONY: deploy-staging
deploy-staging: fetch-mapping
	gcloud -q \
	--project $(STAGING_PROJECT) \
	functions deploy $(FUNCTION_NAME) \
	--region $(REGION) \
	--env-vars-file .env.staging.yaml \
	--runtime $(RUNTIME) \
	--trigger-http \
	--allow-unauthenticated

setup-elasticsearch:
	curl --fail -XPUT "$(ES_URL)/alerts_gstg/_mapping?pretty" -H 'Content-Type: application/json' --data @build/es-mapping.json
	curl --fail -XPUT "$(ES_URL)/alerts_gprd/_mapping?pretty" -H 'Content-Type: application/json' --data @build/es-mapping.json

.PHONY: prod
deploy-prod: fetch-mapping
	gcloud -q --project $(PRODUCTION_PROJECT) beta functions deploy $(FUNCTION_NAME) --region $(REGION) --env-vars-file .env.prod.yaml --runtime $(RUNTIME) --timeout ${TIMEOUT} --trigger-http
