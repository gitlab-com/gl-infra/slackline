const axios = require("axios");

/* eslint-disable  node/no-unpublished-require */ /* js-yaml is a devDependency */
const yaml = require("js-yaml");
const fs = require("fs");

const { SLACKLINE_BASE_URL, BEARER_TOKEN, REGION, STAGING_PROJECT, FUNCTION_NAME, STAGING_ENV_YAML } = process.env;

const FUNCTION_URL = `/${FUNCTION_NAME}Test`;

function getBaseUrl() {
  if (SLACKLINE_BASE_URL) {
    return SLACKLINE_BASE_URL;
  }

  return `https://${REGION}-${STAGING_PROJECT}.cloudfunctions.net`;
}

function getBearerToken() {
  if (BEARER_TOKEN) {
    return BEARER_TOKEN;
  }

  if (!STAGING_ENV_YAML) {
    throw new Error("Integration tests either need STAGING_ENV_YAML or BEARER_TOKEN to be set.");
  }

  const stagingConfig = yaml.load(fs.readFileSync(STAGING_ENV_YAML));

  return stagingConfig.BEARER_TOKEN;
}

const instance = axios.create({
  baseURL: getBaseUrl(),
  timeout: 30000,
  headers: { authorization: `Bearer ${getBearerToken()}` },
});
instance.defaults.headers.post["Content-Type"] = "application/json";

/* eslint-disable no-undef */ /* required for describe */
describe("test-notification", () => {
  describe("#fireTestNotification()", () => {
    it("should communicate with Slackline in the default channel", async () => {
      await postFiringAndResolvedNotifications(FUNCTION_URL);
    });

    it("should communicate with Slackline on alternative channels", async () => {
      await postFiringAndResolvedNotifications(`${FUNCTION_URL}?channel=alerts-gen-svc-test-alt`);
    });

    it("should communicate with Slackline when the resolved message doesn't have a matching firing message", async () => {
      const groupKey = `test_no_firing${Date.now()}`;
      await instance.post(FUNCTION_URL, payload("resolved", groupKey));
    });
  });
});

async function postFiringAndResolvedNotifications(url) {
  const groupKey = `test${Date.now()}`;
  await instance.post(url, payload("firing", groupKey));

  // Give ES time to become consistent
  await new Promise((r) => setTimeout(r, 500));

  await instance.post(url, payload("resolved", groupKey));
}

function payload(status, groupKey) {
  return {
    receiver: "slack_bridge",
    status: status,
    alerts: [
      {
        status: status,
        labels: {
          alertname: "service_ops_out_of_bounds_upper_2sigma_5m",
          bound: "upper",
          environment: "gstg",
          metric: "gitlab_service_ops",
          monitor: "gstg-default",
          period: "5m",
          provider: "gcp",
          region: "us-east",
          replica: "02",
          rules_domain: "general",
          severity: "warn",
          threshold_sigma: "2.5",
          tier: "sv",
          type: "web",
        },
        annotations: {
          description: "Server is running outside of normal operation rate parameters\n",
          grafana_dashboard_id: "general-service/service-platform-metrics",
          grafana_panel_id: "9",
          grafana_variables: "environment,type",
          runbook: "troubleshooting/service-redis.md",
          title: "redis service operation rate alert",
        },
        startsAt: "2020-07-17T08:00:05.09866521Z",
        endsAt: "2020-07-17T08:00:50.09866521Z",
        generatorURL:
          "https://prometheus.gstg.gitlab.net/graph?g0.expr=gitlab_service_ops%3Arate+%3E+gitlab_service_ops%3Arate%3Aavg_over_time_1w+%2B+2.5+%2A+gitlab_service_ops%3Arate%3Astddev_over_time_1w&g0.tab=1",
      },
    ],
    groupLabels: { alertname: "service_ops_out_of_bounds_upper_2sigma_5m", environment: "gstg", tier: "db", type: "redis" },
    commonLabels: {
      alertname: "service_ops_out_of_bounds_upper_2sigma_5m",
      environment: "gstg",
      type: "web",
    },
    commonAnnotations: {
      description: "This is a test alert, fired from Slackline Integration Tests",
      grafana_dashboard_id: "web-main/web-overview",
      grafana_panel_id: "1888663568",
      grafana_datasource_id: "mimir-gitlab-gstg",
      grafana_variables: "environment,type",
      runbook: "troubleshooting/service-redis.md",
      title: "This is a test alert, fired from Slackline Integration Tests",
      link1_title: "Definition",
      link1_url: "https://gitlab.com/gitlab-com/runbooks/blob/master/troubleshooting/definition-service-apdex.md",
      grafana_link1_title: "Components",
      grafana_link1_path: "VE4pXc1iz/general-triage-components",
      grafana_link1_vars: "environment,type",
      grafana_min_zoom_hours: 24,
      promql_template_1: 'gitlab_workhorse_git_http_sessions_active:total{stage="$stage"}',
      promql_template_2: 'avg_over_time(gitlab_workhorse_git_http_sessions_active{type="git", tier="sv", stage="$stage"}[1m])',
    },
    externalURL: "http://alerts-01-inf-gstg:9093",
    version: "4",
    groupKey: groupKey,
  };
}
