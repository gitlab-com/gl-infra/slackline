/* eslint-disable no-undef */
var assert = require("assert");

describe("service-mapping", function () {
  var serviceMapping = require("../../lib/service-mapping");

  describe("#getName()", function () {
    it("should work for valid services", function () {
      let value = serviceMapping.getServiceDetails("web").getName();
      assert.equal(value, "web");
    });

    it("should work for invalid services", function () {
      let value = serviceMapping.getServiceDetails("unknown").getName();
      assert.equal(value, "unknown");
    });
  });

  describe("#getLabel()", function () {
    it("should work for valid services", function () {
      let value = serviceMapping.getServiceDetails("web").getLabel();
      assert(value, "Should return a value");
    });

    it("should work for invalid services", function () {
      let value = serviceMapping.getServiceDetails("unknown").getLabel();
      assert.equal(value, null, "Should not return a value");
    });
  });

  describe("#getGitLabDashboard()", function () {
    it("should work for valid services", function () {
      let value = serviceMapping.getServiceDetails("web").getGitLabDashboard();
      assert(value, "Should return a value");
    });

    it("should work for invalid services", function () {
      let value = serviceMapping.getServiceDetails("unknown").getGitLabDashboard();
      assert.equal(value, null, "Should not return a value");
    });
  });

  describe("#getPrimaryGrafanaDashboard()", function () {
    it("should work for valid services", function () {
      let value = serviceMapping.getServiceDetails("web").getPrimaryGrafanaDashboard();
      assert(value, "Should return a value");
    });

    it("should work for invalid services", function () {
      let value = serviceMapping.getServiceDetails("unknown").getPrimaryGrafanaDashboard();
      assert.equal(value, null, "Should not return a value");
    });
  });

  describe("#getGrafanaFolder()", function () {
    it("should work for valid services", function () {
      let value = serviceMapping.getServiceDetails("gitaly").getGrafanaFolder();
      assert(value, "Should return a value");
    });

    it("should work for invalid services", function () {
      let value = serviceMapping.getServiceDetails("unknown").getGrafanaFolder();
      assert.equal(value, null, "Should not return a value");
    });
  });

  describe("#getSentrySlug()", function () {
    it("should work for valid services", function () {
      let value = serviceMapping.getServiceDetails("web").getSentrySlug();
      assert(value, "Should return a value");
    });

    it("should work for invalid services", function () {
      let value = serviceMapping.getServiceDetails("unknown").getSentrySlug();
      assert.equal(value, null, "Should not return a value");
    });
  });

  describe("#getLoggingLinks()", function () {
    it("should work for valid services", function () {
      let value = serviceMapping.getServiceDetails("web").getLoggingLinks();
      assert(value, "Should return a value");
    });

    it("should work for invalid services", function () {
      let value = serviceMapping.getServiceDetails("unknown").getLoggingLinks();
      assert.equal(value, null, "Should not return a value");
    });
  });
});
