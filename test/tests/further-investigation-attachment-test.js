/* eslint-disable no-undef */
const assert = require("assert");
const furtherInvestigationAttachments = require("../../lib/further-investigation-attachment");

describe("further-investigation-attachment", function () {
  describe("getFurtherInvestigationLinks", () => {
    it("should return valid Grafana explore URLs", () => {
      const payload = require("../data/mimir-payload.json");
      const expected = [
        {
          text: "📈 Alert",
          type: "button",
          url: "https://dashboards.gitlab.net/explore?schemaVersion=1&orgId=1&panes=%7B%2200v%22%3A%7B%22datasource%22%3A%22mimir-gitlab-gstg%22%2C%22queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22expr%22%3A%22gitlab_service_ops%3Arate%20%3E%20gitlab_service_ops%3Arate%3Aavg_over_time_1w%20%2B%202.5%20*%20gitlab_service_ops%3Arate%3Astddev_over_time_1w%22%2C%22range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22uid%22%3A%22mimir-gitlab-gstg%22%7D%2C%22editorMode%22%3A%22code%22%7D%5D%2C%22range%22%3A%7B%22from%22%3A%22now-1h%22%2C%22to%22%3A%22now%22%7D%7D%7D",
        },
        {
          text: "📈 Query",
          type: "button",
          url: "https://dashboards.gitlab.net/explore?schemaVersion=1&orgId=1&panes=%7B%22yzb%22%3A%7B%22datasource%22%3A%22mimir-gitlab-gstg%22%2C%22queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22expr%22%3A%22gitlab_workhorse_git_http_sessions_active%3Atotal%7Bstage%3D%5C%22%5C%22%7D%22%2C%22range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22uid%22%3A%22mimir-gitlab-gstg%22%7D%2C%22editorMode%22%3A%22code%22%7D%5D%2C%22range%22%3A%7B%22from%22%3A%22now-1h%22%2C%22to%22%3A%22now%22%7D%7D%2C%226nt%22%3A%7B%22datasource%22%3A%22mimir-gitlab-gstg%22%2C%22queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22expr%22%3A%22avg_over_time(gitlab_workhorse_git_http_sessions_active%7Btype%3D%5C%22git%5C%22%2C%20tier%3D%5C%22sv%5C%22%2C%20stage%3D%5C%22%5C%22%7D%5B1m%5D)%22%2C%22range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22uid%22%3A%22mimir-gitlab-gstg%22%7D%2C%22editorMode%22%3A%22code%22%7D%5D%2C%22range%22%3A%7B%22from%22%3A%22now-1h%22%2C%22to%22%3A%22now%22%7D%7D%7D",
        },
      ];
      const actual = furtherInvestigationAttachments.getFurtherInvestigationLinks(payload);
      assert.deepEqual(actual, expected);
    });

    it("should return a valid Thanos Prometheus URL", () => {
      const payload = require("../data/thanos-payload.json");
      const expected = [
        {
          text: "📈 Query",
          type: "button",
          url: "https://thanos.gitlab.net/graph?g0.range_input=6h&g0.expr=gitlab_workhorse_git_http_sessions_active%3Atotal%7Bstage%3D%22%22%7D&g0.tab=0&g1.range_input=6h&g1.expr=avg_over_time(gitlab_workhorse_git_http_sessions_active%7Btype%3D%22git%22%2C%20tier%3D%22sv%22%2C%20stage%3D%22%22%7D%5B1m%5D)&g1.tab=0",
        },
      ];
      const actual = furtherInvestigationAttachments.getFurtherInvestigationLinks(payload);
      assert.deepEqual(actual, expected);
    });
  });
});
