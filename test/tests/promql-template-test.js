/* eslint-disable no-undef */
var assert = require("assert");

describe("promql-template", function () {
  var promQLTemplate = require("../../lib/promql-template");
  var alert = require("../data/thanos-payload.json");

  const FIXTURES = [
    ['gitlab_workhorse_git_http_sessions_active:total{environment="$environment"}', 'gitlab_workhorse_git_http_sessions_active:total{environment="gstg"}'],
    ['gitlab_workhorse_git_http_sessions_active:total{environment="$moo"}', 'gitlab_workhorse_git_http_sessions_active:total{environment=""}'],
    ['metric{tier="$tier", environment="$environment"}', 'metric{tier="sv", environment="gstg"}'],
  ];

  describe("#getName()", function () {
    FIXTURES.forEach((t, i) => {
      it(`test ${i}`, function () {
        let render = promQLTemplate(t[0], alert);
        assert.equal(render, t[1]);
      });
    });
  });
});
