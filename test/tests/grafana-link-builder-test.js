/* eslint-disable no-undef */
const assert = require("assert");
const grafanaLinkBuilder = require("../../lib/grafana-link-builder");

describe("grafana-link-builder", () => {
  describe("grafanaLink", () => {
    const payload = require("../data/mimir-payload.json");

    it("should return a valid dashboards URL", () => {
      const expected =
        "https://dashboards.gitlab.net/d/general-service/service-platform-metrics?from=1594886400000&to=1594973700000&panelId=2633741645&tz=UTC&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg&var-type=web";
      const actual = grafanaLinkBuilder.grafanaLink("dashboards.gitlab.net", "/d", {}, payload, true);
      assert.equal(actual, expected);
    });

    it("should return a valid render URL", () => {
      const expected =
        "https://dashboards.gitlab.net/render/d-solo/general-service/service-platform-metrics?from=1594886400000&to=1594972970098&panelId=2633741645&tz=UTC&theme=dark&width=800&height=600&timeout=60&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg&var-type=web";
      const actual = grafanaLinkBuilder.grafanaLink(
        "dashboards.gitlab.net",
        "/render/d-solo",
        {
          theme: "dark",
          width: 800,
          height: 600,
          timeout: 60,
        },
        payload,
        false
      );
      assert.equal(actual, expected);
    });
  });
});
