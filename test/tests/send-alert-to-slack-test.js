/* eslint-disable no-undef */
const assert = require("assert");

describe("promql-template", function () {
  const sendAlertToSlack = require("../../lib/send-alert-to-slack");

  describe("#commonLabelBlocks()", () => {
    it("should handle empty commonLabels", () => {
      let actual = sendAlertToSlack.commonLabelBlocks({ commonLabels: {} });
      assert.deepEqual(actual, []);
    });

    it("should handle long commonLabels", () => {
      let actual = sendAlertToSlack.commonLabelBlocks({
        commonLabels: {
          key: "0123456789012345678901234567890123456789",
        },
      });
      assert.deepEqual(actual, [
        {
          text: {
            text: "key `0123456789012345678901234567890123456789`",
            type: "mrkdwn",
          },
          type: "section",
        },
      ]);
    });

    it("should handle short commonLabels", () => {
      let actual = sendAlertToSlack.commonLabelBlocks({
        commonLabels: {
          key: "012345678",
        },
      });
      assert.deepEqual(actual, [{ type: "section", fields: [{ text: "key `012345678`", type: "mrkdwn" }] }]);
    });

    it("should handle more than 10 short commonLabels", () => {
      let actual = sendAlertToSlack.commonLabelBlocks({
        commonLabels: {
          key1: "012345678",
          key2: "01234567",
          key3: "0123456",
          key4: "012345",
          key5: "01234",
          key6: "0123",
          key7: "012",
          key8: "01",
          key9: "0",
          key10: "12345678",
          key11: "2345678",
        },
      });
      assert.deepEqual(actual, [
        {
          type: "section",
          fields: [
            { text: "key1 `012345678`", type: "mrkdwn" },
            {
              text: "key2 `01234567`",
              type: "mrkdwn",
            },
            {
              text: "key3 `0123456`",
              type: "mrkdwn",
            },
            {
              text: "key4 `012345`",
              type: "mrkdwn",
            },
            {
              text: "key5 `01234`",
              type: "mrkdwn",
            },
            {
              text: "key6 `0123`",
              type: "mrkdwn",
            },
            {
              text: "key7 `012`",
              type: "mrkdwn",
            },
            {
              text: "key8 `01`",
              type: "mrkdwn",
            },
            {
              text: "key9 `0`",
              type: "mrkdwn",
            },
            {
              text: "key10 `12345678`",
              type: "mrkdwn",
            },
          ],
        },
        { type: "section", fields: [{ text: "key11 `2345678`", type: "mrkdwn" }] },
      ]);
    });
  });
});
